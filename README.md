# gnome-shell-extension-gsconnect

KDE Connect implementation with GNOME Shell integration

https://github.com/andyholmes/gnome-shell-extension-gsconnect

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-gsconnect.git
```

